typedef struct sstream sstream;

struct sstream {
  char *format_in;
  char *format_out;
  char *buffer;
  int buffer_size;
  int flags;
};

sstream sstream_init(int);
void sstream_close(sstream);
void sstream_set_format_in(sstream,char*);
void sstream_set_format_out(sstream,char*);
