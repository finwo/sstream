#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "sstream.h"

// ---[ Library functions ]---

sstream sstream_init(int bufsize) {
  sstream output = {.buffer_size = bufsize};
  output.buffer = malloc(bufsize);
  return output;
}

void sstream_close(sstream stream) {
  free(stream.buffer);
}

void sstream_set_format_in(sstream stream, char *format) {
  stream.format_in = format;
}

void sstream_set_format_out(sstream stream, char *format) {
  stream.format_out = format;
}

// ---[ Demo functions ]---

int sstream_print_help(int argc, char **argv) {
  printf("\n");
  printf("This is a library intended to recode raw audio samples\n");
  printf("\n");
  printf("Usage: %s [options]\n", argv[0]);
  printf("\n");
  printf("Options:\n");
  printf("  -a         Print GCC arguments to include this lib\n");
  printf("  -l         Show license\n");
  printf("  -d format  Demo input format\n");
  printf("  -e format  Demo output format\n");
  printf("  -i rate    Demo input rate\n");
  printf("  -o rate    Demo output rate\n");
  return 1;
}

int sstream_print_license(int argc, char **argv) {
  printf("\n");
  printf("MIT License\n");
  printf("\n");
  printf("Copyright (c) 2016 Finwo\n");
  printf("\n");
  printf("Permission is hereby granted, free of charge, to any person obtaining a copy\n");
  printf("of this software and associated documentation files (the \"Software\"), to deal\n");
  printf("in the Software without restriction, including without limitation the rights\n");
  printf("to use, copy, modify, merge, publish, distribute, sublicense, and/or sell\n");
  printf("copies of the Software, and to permit persons to whom the Software is\n");
  printf("furnished to do so, subject to the following conditions:\n");
  printf("\n");
  printf("The above copyright notice and this permission notice shall be included in all\n");
  printf("copies or substantial portions of the Software.\n");
  printf("\n");
  printf("THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR\n");
  printf("IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,\n");
  printf("FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE\n");
  printf("AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER\n");
  printf("LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,\n");
  printf("OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE\n");
  printf("SOFTWARE.\n");
  printf("\n");

  return 0;
}

int main(int argc, char **argv) {
  int opt;
  int flags = 0;
  while((opt=getopt(argc, argv, "al")) != -1) {
    switch(opt) {
      case 'a':
        flags |= 1;
        break;
      case 'l':
        flags |= 2;
        break;
    }
  }

  if ( flags == 0 ) {
    return sstream_print_help(argc, argv);
  }

  if ( flags&2 ) {
    return sstream_print_license(argc, argv);
  }

  return 0;
}
